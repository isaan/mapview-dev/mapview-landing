module.exports = {
  purge: [
    './index.html',
    './public/**/*.html',
    './pages/**/*.{html,vue,js,ts,jsx,tsx,svelte,md}',
  ],
  darkMode: 'class',
}
